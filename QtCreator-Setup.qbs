import qbs

Product {
    name: "QtCreator-Setup"

    files: [
        ".clang-format",
        "build_path.txt",
        "qt_codeformat_template.xml",
    ]
}
